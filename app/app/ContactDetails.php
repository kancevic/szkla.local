<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactDetails extends Model
{
    protected $table = 'contact_details';
    public $timestamps = false;
    protected $fillable =  ['contact_id', 'field_type_id', 'value'];

    public function type()
    {
        return $this->hasOne('App\FieldType', 'id', 'field_type_id');
    }
}
