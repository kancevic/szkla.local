<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FieldType extends Model
{
    protected $table = 'field_types';
    public $timestamps = false;
    protected $fillable =  ['name', 'field_type', 'is_active'];
}
