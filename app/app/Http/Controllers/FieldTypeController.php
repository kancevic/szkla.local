<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\FieldType;
use App\Http\Requests;
use Illuminate\Database\QueryException as QE;

class FieldTypeController extends Controller
{
    public function index()
    {
        $fieldTypes = FieldType::all();
        return view('field-type.index', ['fieldTypes' => $fieldTypes]);
    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:120',
            'field_type' => 'required|max:32',
        ]);

        if ($validator->fails()) {
            return redirect('/field-type')
                ->withErrors($validator);
        }

        $request->is_active
            ? $request->merge(array('is_active' => 1))
            : $request->merge(array('is_active' => 0));

        FieldType::create($request->all());

        return redirect(route('field-type.index'));
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $fieldType = FieldType::find($id);
        if (!$fieldType) {
            return redirect()->back()->withErrors('System error');
        }
        return view('field-type.edit', ['fieldType' => $fieldType]);
    }

    public function update(Request $request, $id)
    {
        $fieldType = FieldType::findOrFail($id);

        $request->is_active
            ? $request->merge(array('is_active' => 1))
            : $request->merge(array('is_active' => 0));

        $fieldType->update($request->all());

        if (!$fieldType) {
            return redirect()->back()->withErrors('System error');
        }

        return redirect(route('field-type.index'));
    }

    public function destroy($id)
    {
        try {
            FieldType::destroy($id);
        } catch(QE $e) {
            $message = 'System error';
            return redirect(route('field-type.index'))->withErrors($message);
        }

        return redirect(route('field-type.index'));
    }
}
