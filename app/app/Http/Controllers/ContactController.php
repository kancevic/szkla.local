<?php

namespace App\Http\Controllers;

use App\ContactDetails;
use App\FieldType;
use Illuminate\Http\Request;
use Validator;
use App\Contact;
use App\Http\Requests;
use Illuminate\Database\QueryException as QE;

class ContactController extends Controller
{
    public function index()
    {
        $contacts = Contact::all();
        return view('contact.index', ['contacts' => $contacts]);
    }

    public function create()
    {
        //
    }

    public function detailStore(Request $request, $id)
    {
        $contact = Contact::find($id);
        if (!$contact) {
            return redirect()->back()->withErrors('System error');
        }

        $validator = Validator::make($request->all(), [
            'contact_id' => 'required|exists:contacts,id',
            'field_type_id' => 'required|exists:field_types,id',
            'value' => 'max:255',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator);
        }

        $fieldType = FieldType::find($request->field_type_id);
        if (!$fieldType->is_active) {
            return redirect()->back()->withErrors('System error');
        }

        ContactDetails::create($request->all());

        return redirect(route('contact.show', $contact->id));
    }

    public function detailDestroy($idContact, $idDetail)
    {
        ContactDetails::destroy($idDetail);
        return redirect()->back();
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'firstname' => 'required|max:128',
            'lastname' => 'max:128',
        ]);

        if ($validator->fails()) {
            return redirect(route('contact.index'))
                ->withErrors($validator);
        }

        Contact::create($request->all());

        return redirect(route('contact.index'));
    }

    public function show($id)
    {
        $contact = Contact::find($id);
        $fieldTypes = FieldType::where('is_active', 1)->get();
        if (!$contact) {
            return redirect(route('contact.index'))->withErrors('System error');
        }
        return view('contact.show', ['contact' => $contact, 'fieldTypes' => $fieldTypes]);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        try {
            Contact::destroy($id);
        } catch(QE $e) {
            $message = 'System error';
            return redirect(route('contact.index'))->withErrors($message);
        }
        return redirect(route('contact.index'));
    }
}
