<?php

Route::get('/', ['as' => 'index', function () {
    return view('welcome');
}]);

Route::resource('field-type', 'FieldTypeController', ['except' => [
    'create', 'show'
]]);

Route::resource('contact', 'ContactController');

Route::post('contact/{id}/detail', [
    'as' => 'contact.detail.store', 'uses' => 'ContactController@detailStore'
]);

Route::delete('contact/{idContact}/detail/{idDetail}', [
    'as' => 'contact.detail.destroy', 'uses' => 'ContactController@detailDestroy'
]);