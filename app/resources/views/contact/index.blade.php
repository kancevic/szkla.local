@extends('layouts.master')

@section('title')
    Welcome!
@endsection

@section('content')
    <h2>Contacts</h2>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>User</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($contacts as $contact)
            <tr>
                <td>{{ $contact->firstname . ' ' . $contact->lastname }}</td>
                <td class="text-center col-md-2">
                    <div class="btn-group">
                        <a class="btn btn-default" href="{{ route('contact.show', $contact->id) }}">Show</a>
                    </div>
                    <div class="btn-group">
                        <form action="{{ route('contact.destroy', $contact->id) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-danger">
                                <i class="fa fa-trash"></i> Delete
                            </button>
                        </form>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <form class="form-inline" role="form" method="POST" action="{{ route('contact.store') }}">
        <div class="form-group">
            <label class="sr-only" for="name">First name:</label>
            <input type="text" name="firstname" class="form-control" id="name" placeholder="First name" value="" />
        </div>
        <div class="form-group">
            <label class="sr-only" for="lastname">Type:</label>
            <input type="text" name="lastname" class="form-control" id="lastname" placeholder="Last name" value="" />
        </div>
        <input type="hidden" name="_token" value="{{ Session::token() }}" />
        <button type="submit" class="btn btn-default">Add new</button>
    </form>
    <br />
@endsection