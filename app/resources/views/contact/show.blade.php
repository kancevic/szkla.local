@extends('layouts.master')

@section('title')
    Welcome!
@endsection

@section('content')
    <h2><a href="{{ route('contact.index') }}">Contact</a> / Show {{ $contact->id }}</h2>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>User</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{ $contact->firstname . ' ' . $contact->lastname }}</td>
                <td class="text-center col-md-3">
                    <div class="btn-group">
                        <a class="btn btn-default" href="{{ route('contact.edit', $contact->id) }}">Edit</a>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <h2>Details</h2>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Type</th>
            <th>Value</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($contact->details as $detail)
            <tr>
                <td>{{ $detail->type->name }}</td>
                <td>{{ $detail->value }}</td>
                <td class="text-center col-md-3">
                    <div class="btn-group">
                        <a class="btn btn-default" href="{{ route('contact.edit', $contact->id) }}">Edit</a>
                    </div>
                    <div class="btn-group">
                        <form action="{{ route('contact.detail.destroy', ['idContact' => $contact->id, 'idDetail' => $detail->id]) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-danger">
                                <i class="fa fa-trash"></i> Delete
                            </button>
                        </form>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <form class="form-inline" role="form" method="POST" action="{{ route('contact.detail.store', $contact->id) }}">
        <div class="form-group">
            <label for="sel1">Type:</label>
            <select class="form-control" id="sel1" name="field_type_id">
                @foreach($fieldTypes as $fieldType)
                    <option value="{{ $fieldType->id }}">{{ $fieldType->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label class="sr-only" for="value">Value:</label>
            <input type="text" name="value" class="form-control" id="value" placeholder="Value" value="" />
        </div>
        <input type="hidden" name="contact_id" value="{{ $contact->id }}" />
        <input type="hidden" name="_token" value="{{ Session::token() }}" />
        <button type="submit" class="btn btn-default">Add new</button>
    </form>
    <br />
@endsection