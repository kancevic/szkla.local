@extends('layouts.master')

@section('title')
    Welcome!
@endsection

@section('content')
    <h2><a href="{{ route('field-type.index') }}">Field types</a> / Edit</h2>
    <form class="form-inline" role="form" method="POST" action="{{ route('field-type.update', $fieldType->id) }}">
        <div class="form-group">
            <label class="sr-only" for="name">Name:</label>
            <input type="text" name="name" class="form-control" id="name" placeholder="Name" value="{{ $fieldType->name }}" />
        </div>
        <div class="form-group">
            <label class="sr-only" for="type">Type:</label>
            <input type="text" name="field_type" class="form-control" id="type" placeholder="Type" value="{{ $fieldType->field_type }}" />
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="is_active" value="1" {{ $fieldType->is_active ? 'checked' : '' }} /> Active</label>
        </div>
        <input type="hidden" name="_token" value="{{ Session::token() }}" />
        <input type="hidden" name="_method" value="PUT" />
        <button type="submit" class="btn btn-default">Edit</button>
    </form>
    <br />
@endsection