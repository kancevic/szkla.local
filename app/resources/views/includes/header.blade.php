<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ route('index') }}">Index</a>
            <a class="navbar-brand" href="{{ route('contact.index') }}">Contacts</a>
            <a class="navbar-brand" href="{{ route('field-type.index') }}">Field Types</a>
        </div>
    </div><!-- /.container-fluid -->
</nav>

@if(count($errors) > 0)
    @foreach($errors->all() as $error)
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{$error}}
        </div>
    @endforeach
@endif