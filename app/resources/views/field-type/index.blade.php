@extends('layouts.master')

@section('title')
    Welcome!
@endsection

@section('content')
    <h2>Field types</h2>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Name</th>
            <th>Type</th>
            <th>Active</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($fieldTypes as $fieldType)
            <tr>
                <td>{{ $fieldType->name }}</td>
                <td>{{ $fieldType->field_type }}</td>
                <td>{{ $fieldType->is_active ? 'Enabled' : 'Disabled' }}</td>
                <td class="text-center col-md-2">
                    <div class="btn-group">
                        <form action="{{ route('field-type.destroy', $fieldType->id) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-danger">
                                <i class="fa fa-trash"></i> Delete
                            </button>
                        </form>
                    </div>
                    <div class="btn-group">
                        <a class="btn btn-default" href="{{ route('field-type.edit', $fieldType->id) }}">Edit</a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <form class="form-inline" role="form" method="POST" action="{{ route('field-type.store') }}">
        <div class="form-group">
            <label class="sr-only" for="name">Name:</label>
            <input type="text" name="name" class="form-control" id="name" placeholder="Name" value="{{ old('name') }}" />
        </div>
        <div class="form-group">
            <label class="sr-only" for="type">Type:</label>
            <input type="text" name="field_type" class="form-control" id="type" placeholder="Type" value="{{ old('field_type') }}" />
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="is_active" value="1" /> Active</label>
        </div>
        <input type="hidden" name="_token" value="{{ Session::token() }}" />
        <button type="submit" class="btn btn-default">Add new</button>
    </form>
    <br />
@endsection